package com.mendez.b142.s02.s02app.controllers;

import com.mendez.b142.s02.s02app.exceptions.UserException;
import com.mendez.b142.s02.s02app.models.User;
import com.mendez.b142.s02.s02app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    UserService userService;

    // Create a new user
    @RequestMapping(value="/users", method = RequestMethod.POST)
    public ResponseEntity<Object> createUser(@RequestBody User newUser) {
        userService.createUser(newUser);
        return new ResponseEntity<>("New user was created", HttpStatus.CREATED);
    }

    // Retrieve all users
    @RequestMapping(value="/users", method=RequestMethod.GET)
    public ResponseEntity<Object> getUsers() {
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

    // Update an existing user
    @RequestMapping(value="/users/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Object> updateUser(@PathVariable Long id, @RequestBody User updatedUser) {
        userService.updateUser(id, updatedUser);
        return new ResponseEntity<>("User details was updated.", HttpStatus.OK);
    }

    // Delete an existing user
    @RequestMapping(value="/users/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Object> deleteUser(@PathVariable Long id){
        userService.deleteUser(id);
        return new ResponseEntity<>("User has been deleted.", HttpStatus.OK);
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<Object> register(@RequestBody Map<String, String> body) throws UserException {
        String username = body.get("username");
        if (!userService.findByUsername(username).isEmpty()) {
            throw new UserException("Username already exists.");
        } else {
            String password = body.get("password");
            String encryptedPassword = new BCryptPasswordEncoder().encode(password);
            User newUser = new User(username, encryptedPassword);
            userService.createUser(newUser);
            return new ResponseEntity<>("New user was registered", HttpStatus.CREATED);
        }
    }
}